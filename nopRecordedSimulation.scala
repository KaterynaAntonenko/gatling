
import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class nopRecordedSimulation extends Simulation {

  private val httpProtocol = http
    .baseUrl("https://demo.nopcommerce.com")
    .inferHtmlResources(AllowList(), DenyList(""".*\.js""", """.*\.css""", """.*\.gif""", """.*\.jpeg""", """.*\.jpg""", """.*\.ico""", """.*\.woff""", """.*\.woff2""", """.*\.(t|o)tf""", """.*\.png""", """.*detectportal\.firefox\.com.*"""))
  
  private val headers_0 = Map(
  		"accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
  		"accept-encoding" -> "gzip, deflate, br",
  		"accept-language" -> "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
  		"sec-ch-ua" -> """ Not A;Brand";v="99", "Chromium";v="98", "Google Chrome";v="98""",
  		"sec-ch-ua-mobile" -> "?0",
  		"sec-ch-ua-platform" -> "Windows",
  		"sec-fetch-dest" -> "document",
  		"sec-fetch-mode" -> "navigate",
  		"sec-fetch-site" -> "none",
  		"sec-fetch-user" -> "?1",
  		"upgrade-insecure-requests" -> "1",
  		"user-agent" -> "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.82 Safari/537.36"
  )
  
  private val headers_1 = Map(
  		"User-Agent" -> "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.82 Safari/537.36",
  		"sec-ch-ua" -> """ Not A;Brand";v="99", "Chromium";v="98", "Google Chrome";v="98""",
  		"sec-ch-ua-mobile" -> "?0",
  		"sec-ch-ua-platform" -> "Windows"
  )
  
  private val headers_4 = Map(
  		"accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
  		"accept-encoding" -> "gzip, deflate, br",
  		"accept-language" -> "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
  		"sec-ch-ua" -> """ Not A;Brand";v="99", "Chromium";v="98", "Google Chrome";v="98""",
  		"sec-ch-ua-mobile" -> "?0",
  		"sec-ch-ua-platform" -> "Windows",
  		"sec-fetch-dest" -> "document",
  		"sec-fetch-mode" -> "navigate",
  		"sec-fetch-site" -> "same-origin",
  		"sec-fetch-user" -> "?1",
  		"upgrade-insecure-requests" -> "1",
  		"user-agent" -> "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.82 Safari/537.36"
  )
  
  private val headers_16 = Map(
  		"accept" -> "*/*",
  		"accept-encoding" -> "gzip, deflate, br",
  		"accept-language" -> "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
  		"sec-ch-ua" -> """ Not A;Brand";v="99", "Chromium";v="98", "Google Chrome";v="98""",
  		"sec-ch-ua-mobile" -> "?0",
  		"sec-ch-ua-platform" -> "Windows",
  		"sec-fetch-dest" -> "script",
  		"sec-fetch-mode" -> "no-cors",
  		"sec-fetch-site" -> "cross-site",
  		"user-agent" -> "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.82 Safari/537.36"
  )
  
  private val headers_19 = Map(
  		"accept" -> "*/*",
  		"accept-encoding" -> "gzip, deflate, br",
  		"accept-language" -> "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
  		"content-type" -> "application/x-www-form-urlencoded; charset=UTF-8",
  		"origin" -> "https://demo.nopcommerce.com",
  		"sec-ch-ua" -> """ Not A;Brand";v="99", "Chromium";v="98", "Google Chrome";v="98""",
  		"sec-ch-ua-mobile" -> "?0",
  		"sec-ch-ua-platform" -> "Windows",
  		"sec-fetch-dest" -> "empty",
  		"sec-fetch-mode" -> "cors",
  		"sec-fetch-site" -> "same-origin",
  		"user-agent" -> "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.82 Safari/537.36",
  		"x-requested-with" -> "XMLHttpRequest"
  )
  
  private val headers_20 = Map(
  		"accept" -> "*/*",
  		"accept-encoding" -> "gzip, deflate, br",
  		"accept-language" -> "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
  		"content-type" -> "text/plain;charset=UTF-8",
  		"origin" -> "https://demo.nopcommerce.com",
  		"sec-ch-ua" -> """ Not A;Brand";v="99", "Chromium";v="98", "Google Chrome";v="98""",
  		"sec-ch-ua-mobile" -> "?0",
  		"sec-ch-ua-platform" -> "Windows",
  		"sec-fetch-dest" -> "empty",
  		"sec-fetch-mode" -> "no-cors",
  		"sec-fetch-site" -> "cross-site",
  		"user-agent" -> "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.82 Safari/537.36"
  )
  
  private val uri1 = "https://api-public.addthis.com/url/shares.json"
  
  private val uri2 = "https://s7.addthis.com/static/sh.f48a1a04fe8dbf021b4cda1d.html"
  
  private val uri3 = "https://v1.addthisedge.com/live/boost/nopsolutions/_ate.track.config_resp"
  
  private val uri5 = "https://m.addthis.com/live/red_lojson"

  private val scn = scenario("nopRecordedSimulation")
    .exec(
      http("request_0")
        .get("/")
        .headers(headers_0)
        .resources(
          http("request_1")
            .get("/js/Homepage.Footer.scripts.js?v=eZlXVOtvTtQGxv8U2tz-F0_vsGY")
            .headers(headers_1),
          http("request_2")
            .get("/Plugins/Widgets.NivoSlider/Scripts/jquery.nivo.slider.js?v=v4esx4-nzB5RcvqrlOrZzpYvBxc")
            .headers(headers_1),
          http("request_3")
            .get("/css/Homepage.Head.styles.css?v=sJef6bO9lRHm1tuNhzK5ikdHRZs")
        )
    )
    .pause(3)
    .exec(
      http("request_4")
        .get("/computers")
        .headers(headers_4)
        .resources(
          http("request_5")
            .get("/css/Category.Head.styles.css?v=RJ69yuaD_LUfr8Ce6wvO72PDy8A"),
          http("request_6")
            .get("/js/Category.Footer.scripts.js?v=UZujW2XIccH1E1lmuAfBbe_3q5Q")
        )
    )
    .pause(2)
    .exec(
      http("request_7")
        .get("/desktops")
        .headers(headers_4)
        .resources(
          http("request_8")
            .get("/js/Category.Footer.scripts.js?v=UZujW2XIccH1E1lmuAfBbe_3q5Q")
            .headers(headers_1),
          http("request_9")
            .get("/css/Category.Head.styles.css?v=RJ69yuaD_LUfr8Ce6wvO72PDy8A")
        )
    )
    .pause(2)
    .exec(
      http("request_10")
        .get("/lenovo-ideacentre-600-all-in-one-pc")
        .headers(headers_4)
        .resources(
          http("request_11")
            .get("/css/Product.Head.styles.css?v=SreZavKB28Ke_85GHF3WKUx5gh0"),
          http("request_12")
            .get("/js/public.estimateshipping.popup.js?v=VMRuZ2TOMqGaLRJk19W9S33uf18"),
          http("request_13")
            .get("/lib_npm/magnific-popup/jquery.magnific-popup.min.js?v=0g-vZHdoii3KuzNrPsjYrDMJfyM"),
          http("request_14")
            .get("/js/Product.Footer.scripts.js?v=FUXJ_M2BLoijfL1rTZY1uyOQ_Ls"),
          http("request_15")
            .get(uri2),
          http("request_16")
            .get(uri3)
            .headers(headers_16),
          http("request_17")
            .get(uri5 + "/300lo.json?si=620a3c8cb7d9629b&bkl=0&bl=1&pdt=514&sid=620a3c8cb7d9629b&pub=nopsolutions&rev=v8.28.8-wp&ln=en&pc=men&cb=0&ab=-&dp=demo.nopcommerce.com&fp=lenovo-ideacentre-600-all-in-one-pc&fr=desktops&of=0&pd=1&irt=1&vcl=1&md=0&ct=1&tct=0&abt=0&cdn=0&pi=1&rb=2&gen=100&chr=UTF-8&mk=open%20source%20ecommerce%2Copen-source%20ecommerce%2CC%23%20shopping%20cart%2CC%23%2Casp.net%20e-commerce%20storefront%2Casp.net%20web%20store%2C.net%20ecommerce%2CC%23%20shopping%20cart%2Cshopping%20cart%2Ce-commerce&colc=1644838028907&jsl=1&uvs=620a3c8cf753faeb000&skipb=1&callback=addthis.cbs.jsonp__84829210330080570")
            .headers(headers_16),
          http("request_18")
            .get(uri1 + "?url=https%3A%2F%2Fdemo.nopcommerce.com%2Flenovo-ideacentre-600-all-in-one-pc&callback=_ate.cbs.sc_httpsdemonopcommercecomlenovoideacentre600allinonepc0")
            .headers(headers_16)
        )
    )
    .pause(2)
    .exec(
      http("request_19")
        .post("/addproducttocart/details/3/1")
        .headers(headers_19)
        .formParam("addtocart_3.EnteredQuantity", "1")
        .formParam("CountryId", "0")
        .formParam("StateProvinceId", "0")
        .formParam("ZipPostalCode", "")
        .formParam("__RequestVerificationToken", "CfDJ8GX72n9ARthDim9hWl9NfE4D67bApqVgl4qRVOlV3cfukIA0GJGqZQB5wMOtgO_NjGKM6L_jUcr15odg-M2g_jTEP76Hw88ibjJ9sq5VdQgYi3TuwadqHvHqvf-USC0zzEFrXIEKIJgMZ-NkIwA9Urg")
    )
    .pause(10)
    .exec(
      http("request_20")
        .post(uri5 + "/100eng.json?sh=1254&ph=2350&ivh=454&dt=13143&pdt=514&ict=&pct=2&perf=widget%7C514%7C251%2Clojson%7C836%7C252%2Csh%7C840%7C14&rndr=render_toolbox%7C891&cmenu=null&ppd=0&ppl=0&fbe=&xmv=0&xms=0&xmlc=0&jsfw=jquery&jsfwv=jquery-3.5.1&al=men&scr=0&scv=0&apiu=0&ba=3&sid=620a3c8cb7d9629b&rev=v8.28.8-wp&pub=nopsolutions&dp=demo.nopcommerce.com&fp=lenovo-ideacentre-600-all-in-one-pc&pfm=0&icns=addthis%2Cfacebook%2Ctwitter%2Cprint%2Cemail")
        .headers(headers_20)
        .resources(
          http("request_21")
            .get("/")
            .headers(headers_4),
          http("request_22")
            .get("/css/Homepage.Head.styles.css?v=sJef6bO9lRHm1tuNhzK5ikdHRZs"),
          http("request_23")
            .get("/Plugins/Widgets.NivoSlider/Scripts/jquery.nivo.slider.js?v=v4esx4-nzB5RcvqrlOrZzpYvBxc"),
          http("request_24")
            .get("/js/Homepage.Footer.scripts.js?v=eZlXVOtvTtQGxv8U2tz-F0_vsGY")
        )
    )
    .pause(7)
    .exec(
      http("request_25")
        .get("/cart")
        .headers(headers_4)
        .resources(
          http("request_26")
            .post("/shoppingcart/checkoutattributechange?isEditable=True")
            .headers(headers_19)
            .formParam("itemquantity11217", "1")
            .formParam("CountryId", "0")
            .formParam("StateProvinceId", "0")
            .formParam("ZipPostalCode", "")
            .formParam("checkout_attribute_1", "1")
            .formParam("discountcouponcode", "")
            .formParam("giftcardcouponcode", "")
            .formParam("__RequestVerificationToken", "CfDJ8GX72n9ARthDim9hWl9NfE5QbupqztRcinezuJ1tfbLSGgp19aYZFmdy9gvFVTRoLyCFHYxW-O3zuu5EP9qzwMPW4ayKnfmP2HAh9lLfhKEdBqNFjIl6v9HrFvk0PRFZ4RaBM_9BCB2MpON-88NSUbM"),
          http("request_27")
            .post("/shoppingcart/selectshippingoption")
            .headers(headers_19)
            .formParam("itemquantity11217", "1")
            .formParam("CountryId", "0")
            .formParam("StateProvinceId", "0")
            .formParam("ZipPostalCode", "")
            .formParam("checkout_attribute_1", "1")
            .formParam("discountcouponcode", "")
            .formParam("giftcardcouponcode", "")
            .formParam("__RequestVerificationToken", "CfDJ8GX72n9ARthDim9hWl9NfE5QbupqztRcinezuJ1tfbLSGgp19aYZFmdy9gvFVTRoLyCFHYxW-O3zuu5EP9qzwMPW4ayKnfmP2HAh9lLfhKEdBqNFjIl6v9HrFvk0PRFZ4RaBM_9BCB2MpON-88NSUbM")
        )
    )

	setUp(scn.inject(atOnceUsers(1))).protocols(httpProtocol)
}
